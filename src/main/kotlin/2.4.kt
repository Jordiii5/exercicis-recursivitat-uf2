import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.next()
    println("El número és creixent? ${creixent(num)}")
}
fun creixent (n:String):Boolean{
    for (i in 0 until n.lastIndex){
        if (n[i]<n[i+1])return creixent(n.drop(1))
        else return false
    }

    return true
}
