import java.util.*
fun main() {
    val scanner= Scanner(System.`in`)
    println("Introdueix el número que vols calcular el factorial:")
    println("El resultat és: ${factorial(scanner.nextLong())}")
}

fun factorial (n:Long):Long{
    if (n>1) return n * factorial((n-1))
    else return 1
}