import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val firstNumber = scanner.nextInt()
    print(asterisc(firstNumber))
}

fun asterisc (n:Int) {
    if (1 < n){
        asterisc(n-1)
    }
    for (i in 1 .. n){
        print("*")
    }
    println()
    if (n>1){
        asterisc(n-1)
    }
}
