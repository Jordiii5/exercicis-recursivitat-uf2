import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()
    println("El resultat és ${reduccio(num)}")
}
fun reduccio (n:Int) :Int{
    if (n < 10) return n
    else return reduccio(n%10 + reduccio(n/10))
}
