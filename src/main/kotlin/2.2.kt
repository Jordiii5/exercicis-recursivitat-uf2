import java.util.*
fun main() {
    val scanner= Scanner(System.`in`)
    println("Introdueix el número que vols calcular el doble factorial:")
    println("El resultat és: ${doubleFactorial(scanner.nextLong())}")
}

fun doubleFactorial(n:Long):Long{
    if (n>1) return n * doubleFactorial(n-2)
    else return 1
}