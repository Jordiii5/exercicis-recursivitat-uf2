import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()
    var divisors=0
    var resultat= primersPerfectes(num)
    if (resultat/10 != 0){
        resultat= primersPerfectes(resultat)
    }
    if (resultat/10 == 0){
        for (i in 1..resultat){
            if (resultat%i ==0){
                divisors++
            }
        }
        if (divisors==2){
            println(true)
        }
        else {
            println(false)}
    }
}

fun primersPerfectes(n:Int):Int{
    var resultat:Int
    if (n!=0){
        resultat= n%10 + primersPerfectes(n/10)
    }
    else{
        return 0
    }
    return resultat
}
