import java.util.*
fun main() {
    val scanner= Scanner(System.`in`)
    println("Introdueix un número:")
    println("El número té: ${digits(scanner.nextInt())} dígits")
}
fun digits (n:Int):Int{
    if (n<10) return 1
    else return 1 + digits(n/10)
}