import java.util.*

fun main (){
    val scanner = Scanner (System.`in`)
    println("Numero de discos:")
    val number = scanner.nextInt()
    println("${hanoi(number,'A','B','C')}")
}

fun hanoi (n:Int, palo1:Char, palo2:Char, palo3:Char){
    if (n == 1){
        println ("$palo1 => $palo3")
    }else{
        hanoi(n-1, palo1, palo3, palo2)
        println("$palo1 => $palo3")
        hanoi(n-1, palo2, palo1, palo3)
    }
}